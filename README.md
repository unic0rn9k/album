# Album

The album lib started as a part of my deepderp lib, and has now branched out to its own lib. The point of the album lib is to get vector and matrix math together in one data type in go lang. I decided to just make my own lib, since I couldn't find any good vector math libs made for go.

NumAlbum is a float array with some special matrix and vector opperators

### install:

`go get gitlab.com/unic0rn9k/album`

