package album

import (
	"fmt"
	"math/rand"
)

/*
  #
 #:#                  ##
 #: #    ###   ####  #: #
 #:  #  ##: #  #: #  #: #
 #:   # #:  #  #: #  #: #
 #:   # ####  #####  ###
 #:   # #:    #: #   #:
 #:  ## #:    #:  #  #:
 #:  #   ###  #:   # #:
 ####         #:     #:
                     #:

*/

//TODO: make all album functions that return nothing, operate on a non pointer and return the result instead

//NumAlbum is a float array with some special matrix and vector opperators
type NumAlbum []float64

//AsAlbum converts a float array into an album
func AsAlbum(in []float64) NumAlbum {
	album := NumAlbum{}
	album = in
	return album
}

//NewNumAlbum returns a empty album of specified size
func NewNumAlbum(size int) NumAlbum {
	return AsAlbum(make([]float64, size))
}

//AppendAlbum adds an album to the end of an existing album
func (album *NumAlbum) AppendAlbum(input ...NumAlbum) {
	for _, in := range input {
		newAlbum := AsAlbum(make([]float64, len(*album)+len(in)))
		for n := 0; n < len(*album); n++ {
			newAlbum[n] = (*album)[n]
		}
		for n := len(*album); n < len(newAlbum); n++ {
			newAlbum[n] = in[n]
		}
		*album = newAlbum
	}
}

//Add adds a float to a album
func (album NumAlbum) Add(in float64) NumAlbum {
	newAlbum := AsAlbum(make([]float64, len(album)))
	for n := range album {
		newAlbum[n] = album[n] + in
	}
	return newAlbum
}

//TODO: scaled album of scaled a scaled album is inefficient! make a scale function that can take an album as an input

//Scale scales an album as if it was a vector (it multiplies an album with a float)
func (album NumAlbum) Scale(in float64) NumAlbum {
	newAlbum := AsAlbum(make([]float64, len(album)))
	for n := range album {
		newAlbum[n] = album[n] * in
	}
	return newAlbum
}

//AddAlbum adds two albums together
func (album *NumAlbum) AddAlbum(in ...NumAlbum) error {
	for _, nOfIn := range in {
		if len(*album) != len(nOfIn) {
			return fmt.Errorf("Unable to add albums of diffrent length")
		}
		for y := range nOfIn {
			(*album)[y] += nOfIn[y]
		}
	}
	return nil
}

//MultiplyAlbum multipolyes two albums together
func (album *NumAlbum) MultiplyAlbum(in ...NumAlbum) error {
	for _, nOfIn := range in {
		if len(*album) != len(nOfIn) {
			return fmt.Errorf("Unable to add albums of diffrent length")
		}
		for y := range nOfIn {
			(*album)[y] *= nOfIn[y]
		}
	}
	return nil
}

//Sum returns sum of all elements in album
func (album NumAlbum) Sum() float64 {
	var sum float64
	for n := range album {
		sum += album[n]
	}
	return sum
}

//DotAlbum return dot product of two albums
func DotAlbum(x NumAlbum, y NumAlbum) float64 {
	z := x
	if err := z.MultiplyAlbum(y); err != nil {
		panic(err)
	}
	return z.Sum()
}

//DimDotAlbum returns a album of dot products and takes two album arrays as an argument
func DimDotAlbum(xs []NumAlbum, ys []NumAlbum) NumAlbum {
	if len(xs) != len(ys) {
		panic(fmt.Errorf("Unable to find dot album of album arrays of diffrent size"))
	}

	albumSum := AsAlbum(make([]float64, len(xs)))
	for n := range xs {
		albumSum[n] = DotAlbum(xs[n], ys[n])
	}
	return albumSum
}

//IsEqualTo compares to albums and returns a bool that is true if they are the same and false if not
func (album NumAlbum) IsEqualTo(x NumAlbum) bool {
	if len(album) != len(x) {
		return false
	}
	isEqual := true
	for n := 0; n < len(album) && isEqual; n++ {
		if album[n] != x[n] {
			isEqual = false
		}
	}
	return isEqual
}

//RandomNumAlbum returns a randomly generated album
func RandomNumAlbum(length int, scaler float64) NumAlbum {
	out := AsAlbum(make([]float64, length))
	for n := range out {
		out[n] = []float64{-1, 1}[rand.Intn(2)] * rand.Float64()
	}
	return out.Scale(scaler)
}

//AlbumFillWith returns an album filled with one specified float
func AlbumFillWith(x float64, length int) NumAlbum {
	out := AsAlbum(make([]float64, length))
	for n := range out {
		out[n] = x
	}
	return out
}
